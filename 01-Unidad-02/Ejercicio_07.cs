﻿using System;

namespace Unidad_02
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Ejercicio: Salario
                En una empresa se estipula un sueldo básico de $1200 para quienes tienen categoría D o E
                y una antigüedad menor a 5 años y trabajaron por lo menos 160 horas en el mes; quienes
                no alcancen las 160 horas mensuales tendrán un básico de $1000.
                Los empleados de categoría C con una antigüedad menor a 5 años que trabajaron por lo
                menos 160 horas en el mes recibirán $2200, quienes no alcancen las 160 horas
                mensuales tendrán un básico de $2000.
                Los empleados de categorías A o B con una antigüedad menor a 5 años que trabajaron por
                lo menos 160 horas en el mes recibirán $4200, quienes no alcancen las 160 horas
                mensuales tendrán un básico de $4000.
                En todos los casos, si la antigüedad es mayor a 5 años, sumar un 1% por año extra.
                Se pide crear un programa que permita calcular el salario según la situación laboral
                ingresada. Los datos a ingresar serán: categoría, horas mensuales y antigüedad.
             */

            /* Declaracion de variables */
            char inputCategory;
            short inputWorkHours;
            byte inputSeniority;
            float salary;

            /* Declaracion de constantes */
            const char CATEGORY_A = 'A';
            const char CATEGORY_B = 'B';
            const char CATEGORY_C = 'C';
            const char CATEGORY_D = 'D';
            const char CATEGORY_E = 'E';
            const short BASIC_SALARY_CATEGORY_A_B = 4000;
            const short BASIC_SALARY_CATEGORY_C = 2200;
            const short BASIC_SALARY_CATEGORY_D_E = 1200;
            const byte MIN_SENIORITY = 5;
            const byte LIMIT_WORK_HOURS = 160;
            const float SENIORITY_PORCENTAGE = 0.01f;
            const short PLUS_160_HOURS = 200;


            /* Peticion de ingreso de datos del empleado por consola */
            Console.WriteLine("Ingrese categoria");
            inputCategory = char.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese horas mensuales realizadas");
            inputWorkHours = short.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese antiguedad");
            inputSeniority = byte.Parse(Console.ReadLine());

            /* Camparo la categoria ingresada */
            if (inputCategory == CATEGORY_A || inputCategory == CATEGORY_B)
            {
                salary = BASIC_SALARY_CATEGORY_A_B;
            }
            else if (inputCategory == CATEGORY_C)
            {
                salary = BASIC_SALARY_CATEGORY_C;
            }
            else if (inputCategory == CATEGORY_D || inputCategory == CATEGORY_E)
            {
                salary = BASIC_SALARY_CATEGORY_D_E;
            }

            /* Comparo la horas trabajadas */
            if (inputWorkHours == LIMIT_WORK_HOURS)
            {
                salary += PLUS_160_HOURS;
            }

            /* Comparo la antiguedad */
            if (inputSeniority == MIN_SENIORITY)
            {
                while (inputSeniority >= MIN_SENIORITY)
                {
                    /* Declaracion de variable auxiliar */
                    float seniorityValue;

                    /* Calculo del valor del porcentaje de antiguedad */
                    seniorityValue = salary * SENIORITY_PORCENTAGE;

                    /* Calculo del salario + antiguedad */
                    salary += seniorityValue;

                    inputSeniority--;
                }
            }

            Console.WriteLine("El salario a recibir es: {0}", salary);
        }
    }
}
