﻿using System;

namespace Unidad_02
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Ejercicio 3: Gestión de Usuario
                Escribir un programa que:
                ● Pida un usuario.
                ● Pida una contraseña.
                ● Verifique si el usuario y contraseña son válidos (se considera un usuario válido aquel
                cuyo valor sea “admin” y contraseña válida aquella cuyo valor sea “1234”)
                ● Si el usuario no es válido informar “Usuario inválido”.
                ● Si el usuario es válido pero la contraseña no lo es, informar “Contraseña inválida”.
                ● Si usuario y contraseña son válidos informar “Bienvenido” seguido del nombre del
                usuario.
             */

            /* Declaracion de variables */
            string username;
            string password;

            /* Declaracion de constantes */
            const string VALID_USERNAME = "admin";
            const string VALID_PASSWORD = "1234";

            /* Pedido e ingreso de datos por consola */
            Console.WriteLine("Ingrese usuario: ");
            username = Console.ReadLine();
            Console.WriteLine("Ingrese clave: ");
            password = Console.ReadLine();

            /* Verificacion de validez de usuario y contraseña */
            if (username == VALID_USERNAME && password == VALID_PASSWORD)
            {
                Console.WriteLine("Bienvenido {}", username);
            }
            else if (username != VALID_USERNAME)
            {
                Console.WriteLine("Usuario invalido");
            }
            else if (username == VALID_USERNAME && password != VALID_PASSWORD)
            {
                Console.WriteLine("Contraseña invalida");
            }
        }
    }
}
