﻿using System;

namespace Unidad_02
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Ejercicio: Hacer un programa que simule un cajero automático. Para esto, primero se pedirá al
                usuario que indique que tarea quiere realizar mediante el ingreso de un número:
                1. Ingresar dinero en la cuenta
                2. Extraer dinero de la cuenta
                3. Ver saldo disponible
                0. Salir
                Considerar que el programa inicia con un saldo inicial en la cuenta de $1000.
                Resolverlo sin utilizar la estructura “switch”
             */

            /* Declaracion de constantes */
            const decimal BEGINNING_BALANCE = 1000;
            /* Declaracion de variables */
            byte operation;
            decimal inputMoney;
            decimal balance = BEGINNING_BALANCE;

            /* Carga de Datos */
            Console.WriteLine("Bienvenido al cajero automatico");
            /* Carga de Datos */
            Console.WriteLine("Seleccione una opcion:");
            Console.WriteLine("1. Ingresar dinero en cuenta\n2. Extracción\n3. Ver saldo disponible\n0. Salir\n\nUsted ha marcado: ");
            operation = byte.Parse(Console.ReadLine());

            /* Eleccion de opciones */
            if (operation == 1)
            {
                Console.WriteLine("Ingrese el monto a depositar: ");

                inputMoney = decimal.Parse(Console.ReadLine());
                balance += inputMoney;

                Console.WriteLine("Operacion exitosa. Su saldo actual es de {0}", balance);
            }
            else if (operation == 2)
            {
                Console.WriteLine("\nIngrese el monto a retirar: ");

                inputMoney = decimal.Parse(Console.ReadLine());

                /* Validacion: dinero a extraer mayor al saldo disponible */
                if (inputMoney > balance)
                    Console.WriteLine("\nError. Saldo no disponible.\n");
                else
                {
                    balance -= inputMoney;
                    Console.WriteLine("\nOperacion exitosa. Su saldo actual es de {0}\n", balance);
                }

            }
            else if (operation == 3)
                Console.WriteLine("\nSu saldo actual es de {0}\n", balance);

            else if (operation == 0)
                Console.WriteLine("\nGracias por utilizar este programa.\n\n");

            else
                Console.WriteLine("\nDisculpe, opcion incorrecta.\n\n");
        }
    }
}
