﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unidad-02
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Ejercicio 2: Colores RGB
                Ingresar un valor entre 0 y 255.
                0 a 100: Rojo
                101 a 180: Verde
                181 a 255: Azul
                Informar cuál fue el color ingresado
             */

            /* Declaracion de variables */
            byte inputData;

            /* Declaracion de constantes */
            const byte LOWER_LIMIT_RED_COLOUR = 0;
            const byte UPPER_LIMIT_RED_COLOUR = 100;
            const byte LOWER_LIMIT_GREEN_COLOUR = 101;
            const byte UPPER_LIMIT_GREEN_COLOUR = 180;
            const byte LOWER_LIMIT_BLUE_COLOUR = 181;
            const byte UPPER_LIMIT_BLUE_COLOUR = 255;

            /* Pedido e ingreso de valor por consola */
            Console.WriteLine("Ingrese un valor entre 0 y 255");
            inputData = byte.Parse(Console.ReadLine());

            /* Comparacion de valor ingresado y se informa el resultado por consola*/
            if (inputData >= LOWER_LIMIT_RED_COLOUR && inputData <= UPPER_LIMIT_RED_COLOUR)
            {
                Console.WriteLine("El valor ingresado corresponde al color rojo");
            }
            else if (inputData >= LOWER_LIMIT_GREEN_COLOUR && inputData <= UPPER_LIMIT_GREEN_COLOUR)
            {
                Console.WriteLine("El valor ingresado corresponde al color verde");
            }
            else if (inputData >= LOWER_LIMIT_BLUE_COLOUR && inputData <= UPPER_LIMIT_BLUE_COLOUR)
            {
                Console.WriteLine("El valor ingresado corresponde al color azul");
            }
        }
    }
}
