﻿using System;

namespace Unidad_02
{
    class Program
{
    static void Main(string[] args)
    {
        /* Ejercicio 10: Búsqueda de mayor
            Escribir un programa que analice cual de cuatro variables tipo int contiene el mayor valor y
            luego muestre en pantalla el nombre de dicha variable.
         */

        /* Declaracion de variables */
        int inputNumber;
        int biggerNumber = int.MinValue;

        /* Declaración de constante */
        const byte QuantityVar = 4;

        for (int i = 1; i <= QuantityVar; i++)
        {
            /* Pedido de valores enteros por consola */
            Console.WriteLine("Ingrese el {0}° numero:",i);
            inputNumber = int.Parse(Console.ReadLine());

            /* Comparacion de valores y busqueda del mayor valor */
            if (biggerNumber < inputNumber)
            {
                    biggerNumber = inputNumber;
            }
        }

        /* Se muestra por pantalla */
        Console.WriteLine("El mayor numero ingresado es: {0}", biggerNumber);
    }
}
}
