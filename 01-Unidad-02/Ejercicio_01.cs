using System;

namespace Unidad_02
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Ejercicio 1: Identificador de Blanco y Negro
                Ingresar por consola un valor entre 0 y 255.
                El valor 255 equivale al color blanco, cualquier otro valor equivale a negro.
                a) Informar por pantalla si el valor ingresado corresponde al blanco
                b) Informar si el valor ingresado corresponde a blanco o en caso contrario, informar que
                corresponde a negro.*/

            /* Declaracion de variable */
            byte inputData;

            /* Declaracion de constante*/
            const byte WHITE_COLOUR = 255;

            /* Pedido e ingreso de valor por consola */
            Console.WriteLine("Ingrese un valor entre 0 y 255");
            inputData = byte.Parse(Console.ReadLine());

            /* Comparacion de valor ingresado y se informa el resultado por consola*/
            if (inputData == WHITE_COLOUR)
            {
                Console.WriteLine("El valor ingresado corresponde al color blanco");
            }
            else
            {
                Console.WriteLine("El valor ingresado corresponde al color negro");
            }
        }
    }
}
