﻿using System;

namespace Unidad_02
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Ejercicios 5: Gestion de usuario (Varios usuarios)
                Modificar el ejercicio 3 para que pueda trabajar con los siguientes usuarios válidos:
                usuario1: juan clave1: 1234
                usuario2: laura clave2: abcd
                usuario3: miguel clave3: AsDf
                usuario4: paula clave4: 3333
            */

            /* Declaracion de variables */
            string username;
            string password;

            /* Declaracion de constantes */
            const string VALID_USERNAME1 = "juan";
            const string VALID_PASSWORD1 = "1234";
            const string VALID_USERNAME2 = "laura";
            const string VALID_PASSWORD2 = "abcd";
            const string VALID_USERNAME3 = "miguel";
            const string VALID_PASSWORD3 = "AsDf";
            const string VALID_USERNAME4 = "paula";
            const string VALID_PASSWORD4 = "3333";

            /* Pedido e ingreso de datos por consola */
            Console.WriteLine("Ingrese usuario: ");
            username = Console.ReadLine();
            Console.WriteLine("Ingrese clave: ");
            password = Console.ReadLine();

            /* Verificacion de validez de usuario y contraseña */
            if (username == VALID_USERNAME1)
            {
                if (password == VALID_PASSWORD1)
                {
                    Console.WriteLine("Bienvenido {}", username);
                }
                else
                {
                    Console.WriteLine("Contraseña invalida");
                }    
            }
            else if (username == VALID_USERNAME2)
            {
                if (password == VALID_PASSWORD2)
                {
                    Console.WriteLine("Bienvenido {}", username);
                }
                else
                {
                    Console.WriteLine("Contraseña invalida");
                }
            }
            else if (username == VALID_USERNAME3)
            {
                if (password == VALID_PASSWORD3)
                {
                    Console.WriteLine("Bienvenido {}", username);
                }
                else
                {
                    Console.WriteLine("Contraseña invalida");
                }    
            }
            else if (username == VALID_USERNAME4)
            {
                if (password == VALID_PASSWORD4)
                {
                    Console.WriteLine("Bienvenido {}", username);
                }
                else
                {
                    Console.WriteLine("Contraseña invalida");
                }
            }
            else
            {
                Console.WriteLine("Usuario invalido");
            }
        }
    }
}
